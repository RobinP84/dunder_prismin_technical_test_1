﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace dunder_prismin_technical_test_1 {
    class Program {
        static void Main(string[] args) {
            var text = File.ReadAllLines(@".\..\..\..\..\words-english.txt");
            var fourLetterWords = RemoveNonFourLetterWords(text);
            var startWord = "Alex"; 
            var endWord = "Aida"; 
            if (args.Length > 0)
                startWord = args[0];
            if (args.Length > 1)
                endWord = args[1];

            var wordsMap = new List<List<string>>() { new List<string> { startWord } };
            fourLetterWords.Remove(startWord);
            wordsMap = MapWords(endWord, fourLetterWords, wordsMap);
            var answer = FindPaths(wordsMap, new List<List<string>>() { new List<string>(wordsMap[wordsMap.Count - 1]) }, 0, wordsMap.Count - 2);
            PrintResult(answer, "ResultFile", false);
            Console.WriteLine();
        }
        static void PrintResult(List<List<string>> answer, string fileName, bool writeToConsole) {
            var headline = "The shortest path between " + answer[0][answer[0].Count - 1] + " and " + answer[0][0] + " is:\n";
            if (answer.Count > 1)
                headline = "There are " + answer.Count + " equally short paths between " + answer[0][answer[0].Count - 1] + " and " + answer[0][0] + ". They are: \n";
            using (StreamWriter resultFile =
            new StreamWriter(@".\..\..\..\..\" + fileName + ".txt")) {
                resultFile.WriteLine(headline);
                if (writeToConsole) 
                    Console.WriteLine(headline);
                foreach (var path in answer) {
                    for (int i = path.Count - 1; i >= 0; i--) {
                        resultFile.WriteLine(path[i]);
                        if (writeToConsole) 
                            Console.WriteLine(path[i]);
                    }
                    resultFile.WriteLine();
                    if (writeToConsole) 
                        Console.WriteLine();
                }
            }
        }
        static List<string> CopyBetweenRange(List<string> list, int start, int end) {
            var newList = new List<string>();
            for (int i = start; i <= end; i++) {
                newList.Add(list[i]);
            }
            return newList;
        }
        static List<List<string>> FindPaths(List<List<string>> wordMap, List<List<string>> paths, int p, int l) {
            for (; l >= 0; l--) { //För varje lista i wordsMap
                var word = paths[p].Last();
                for (int w = 0, matchingWords = 0; w < wordMap[l].Count; w++) { //För varje ord i wordsMap[l]
                    if (DiffOneLetterFromWord(word, wordMap[l][w])) { //Om sista ordet i listan paths[p] skiljer från ordert wordsMap[l][w] med max en bokstav
                        matchingWords++;
                        if (matchingWords > 1) {
                            var newPath = CopyBetweenRange(paths[p], 0, paths[p].Count - 2);
                            newPath.Add(wordMap[l][w]);
                            paths.Add(newPath);
                            paths = FindPaths(wordMap, paths, paths.Count-1, l-1);
                        }
                        else
                            paths[p].Add(wordMap[l][w]);
                    }
                }
            }
            return paths;
        }        
        static List<List<string>> MapWords(string endWord, List<string> dictionary, List<List<string>> wordMap) {
            var endWordFound = false;
            while (dictionary.Count > 0 && !endWordFound) {
                var words = new List<string>();
                for (int i = 0; i < wordMap[wordMap.Count - 1].Count; i++) {
                    if (DiffOneLetterFromWord(wordMap[wordMap.Count - 1][i], endWord)) {
                        words = new List<string>() { endWord };
                        endWordFound = true;
                        break;
                    }
                    else {
                        for (int j = 0; j < dictionary.Count; j++) {
                            if (DiffOneLetterFromWord(wordMap[wordMap.Count - 1][i], dictionary[j])) {
                                words.Add(dictionary[j]);
                                dictionary.Remove(dictionary[j]);
                                j--;
                            }
                        }
                    }
                }
                wordMap.Add(words);
                if (words.Count <= 0)
                    throw new Exception("There is no path that leads to " + endWord);
            }
            return wordMap;
        }
        static bool DiffOneLetterFromWord(string startWord, string endWord) {
            int lettersDiffering = 0;
            for (int i = 0; i < startWord.Length; i++) {
                if (endWord[i] != startWord[i]) {
                    ++lettersDiffering;
                    if (lettersDiffering > 1) {
                        return false;
                    }
                }
            }
            return true;
        }
        static List<string> RemoveNonFourLetterWords(string[] words) {
            var newWords = new List<string>();
            for (int i = 0; i < words.Length; i++) {
                if (words[i].Length == 4 && !StringIsNotWord(words[i])) {
                    newWords.Add(words[i]);
                }
            }
            return newWords;
        }
        static bool StringIsNotWord(string word) {
            foreach (var character in word) {
                if (char.GetUnicodeCategory(character) != UnicodeCategory.LowercaseLetter) {
                    if (char.GetUnicodeCategory(character) != UnicodeCategory.UppercaseLetter) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}